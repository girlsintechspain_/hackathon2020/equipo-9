from django.shortcuts import render, redirect
from .models import *
from django.urls import reverse_lazy
from django.views.generic import *
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.views import generic

from .models import Choice, Question, Name
from .forms import NameForm
from django.utils import timezone


def index(request):
    names_from_db = Name.objects.all()

    form = NameForm()

    context_dict = {'names_from_context': names_from_db, 'form': form}

    if request.method == 'POST':
        form = NameForm(request.POST)

        if form.is_valid():
            form.save(commit=True)
            return render(request, 'index.html', context_dict)
        else:
            print(form.errors)    

    return render(request, 'index.html', context_dict)


def pedir_cita(request):
    return render(request, 'cita.html', {})


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        """
        return Question.objects.filter(
            pub_date__lte=timezone.now()
        ).order_by('-pub_date')[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))


class AboutUs(TemplateView):
    model = Nosotros
    template_name = 'polls/nosotros.html'   
    context_object_name = 'nos'
    queryset = Nosotros.objects.all() 

   
    def get_context_data(self, **kwargs):
        context = super(AboutUs, self).get_context_data(**kwargs)
        context['nos'] = Nosotros.objects.all()
        #context['anual'] = ProyectoAnual.objects.all()
        return context


class Especialidades(TemplateView):
    model = Especialidad
    template_name = 'polls/Especialidades.html'   
    context_object_name = 'nos'
    queryset = Especialidad.objects.all() 

   
    def get_context_data(self, **kwargs):
        context = super(Especialidades, self).get_context_data(**kwargs)
        context['nos'] = Nosotros.objects.all()
        context['anual'] = Especialidad.objects.all()
        return context


class Citacion(TemplateView):
    model = Citados
    template_name = 'polls/cita.html'   
    context_object_name = 'nos'
    queryset = Citados.objects.all() 

   
    def get_context_data(self, **kwargs):
        context = super(Citacion, self).get_context_data(**kwargs)
        context['nos'] = Nosotros.objects.all()
        context['anual'] = Especialidad.objects.all()
        return context
