#from django.urls import path

#from . import views



from django.contrib import admin
from django.urls import path

from polls import views
from django.contrib.auth.decorators import login_required
from django.conf.urls import url, include


app_name = 'polls'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
    path('nosotros/', views.AboutUs.as_view(), name='nosotros'), 
    path('cita/', views.pedir_cita, name='cita'), 
    path('especialidad/', views.Especialidades.as_view(), name='especialidades'),
    path('citas/', views.Citacion.as_view(), name='citados'),
]



